export class User {
    _mobileNumber: string;
    _city: string;
    _email: string;
    username: any;
    _address: string;
    _pinCode: number;
    _createdAt: string;
    _state: string;
    _password: string;
    _isAdmin: true;
}
