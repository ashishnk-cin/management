export class Tailor {

    // Basic information
    '_partnerShop': string;
    '_contactPerson': string;
    '_designation': string;
    '_contactNumber': number;
    '_emailTailor': string;
    '_whatsappNumber': number;
    '_specialisation': string;

    //address information
    '_state': string;
    '_city': string;
    '_zipcode': string;
    '_fullAddress': string;

    //more information
    '_yearEstablished': string;
    '_service': string;
    '_daysRegularDelivery': number;
    '_daysExpressDelivery': number;

    //rate card men
    '_rateCard': {
        '_priceShirt': number;
        '_priceTrouser': number;
        '_priceJacket': number;
        '_priceWaistCoat': number;
        '_priceSuit': number;
        '_priceGroomWear': number;
    }

    //Both the passwords are in a single object
    'password': {
      'pwd': string;
      'confirmPwd': string;
    };

    'gender': string;
    'terms': boolean;
 
    constructor(values: Object = {}) {

      //Constructor initialization
      Object.assign(this, values);
  }
 
}