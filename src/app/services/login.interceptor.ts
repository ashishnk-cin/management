import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw'
import 'rxjs/add/operator/catch';

@Injectable()
export class LoginInterceptor implements HttpInterceptor {

    constructor() {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log('intercepted request ... ');

            request = request.clone({
                setHeaders: {
                  Authorization: `Bearer ${localStorage.getItem('token')}`
                }
              });

            // send the newly created request
            return next.handle(request)

            .catch((error, caught) => {
            // intercept the respons error and displace it to the console
            console.log('Error Occurred');
            console.log(error);
            // return the error to the method that called it
            return Observable.throw(error);
            }) as any;
        }
    }
