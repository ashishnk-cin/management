import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class TailorService {
  serviceUrl = 'http://ec2-13-127-44-90.ap-south-1.compute.amazonaws.com:4040/api/inputs';
  tailorlistinput = 'http://ec2-13-127-44-90.ap-south-1.compute.amazonaws.com:4040/api/inputs?type=stiching';

  constructor(private http: HttpClient) { }

  addTailor(details) {
    const datadetail = {inputs: [ details ] };
    console.log('this is body', datadetail);
    this.http.post(this.serviceUrl, datadetail).subscribe(
      data => {
       console.log('Partner Registration is completed');
      },
      error => {
        console.log('Error occured at addTailor Service:', error);
      });
    console.log('Post method ran with these details:', datadetail);
  }

  getAll() {
   return this.http.get(this.tailorlistinput);
  }
}
