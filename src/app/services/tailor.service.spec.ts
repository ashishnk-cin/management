import { TestBed, inject } from '@angular/core/testing';

import { TailorService } from './tailor.service';

describe('TailorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TailorService]
    });
  });

  it('should be created', inject([TailorService], (service: TailorService) => {
    expect(service).toBeTruthy();
  }));
});
