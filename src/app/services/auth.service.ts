import { tokenNotExpired, JwtHelper } from 'angular2-jwt';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable()
export class AuthService {
    token: string;
    loginUrl = 'http://ec2-13-127-44-90.ap-south-1.compute.amazonaws.com:4040/api/auth/login';

  constructor(private http: HttpClient,
        private router: Router,
        private route: ActivatedRoute) { }

    loginA(credentials) {
        console.log('AuthService-Username:', credentials);
        return this.http.post<any>(this.loginUrl, credentials)
            .map(response => {
                console.log('This is response from server', response.json);
                const result = response;
                    // login successful if there's a jwt token in the response
                    if (result && result.token) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('token', result.token);
                return true;
                } else {
                    return false;
                }
            });
    }

    public getToken(): string {
        return localStorage.getItem('token');
      }


    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('token');
        this.router.navigate(['/']);
    }

    isLoggedIn() {
        return tokenNotExpired();
    }

    get currentUser() {
        const token: string = localStorage.getItem('token');
        if (!token) { return null; }
        return  new JwtHelper().decodeToken(token);
    }
}
