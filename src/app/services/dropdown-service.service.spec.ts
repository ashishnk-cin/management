import { TestBed, inject } from '@angular/core/testing';

import { DropdownServiceService } from './dropdown-service.service';

describe('DropdownServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DropdownServiceService]
    });
  });

  it('should be created', inject([DropdownServiceService], (service: DropdownServiceService) => {
    expect(service).toBeTruthy();
  }));
});
