import { Specialization } from './../model/specialization';
import { Category } from './../model/category';
import { Injectable } from '@angular/core';
import { Businesstype } from 'app/model/businessType';
import { Businesscategories } from 'app/model/businessCategory';


@Injectable()
export class DropdownServiceService {

  getCategory() {
    return [
     new Category(1, 'Tailoring' ),
     new Category(2, 'Men Fabric' ),
     new Category(3, 'Women Fabric' ),
     new Category(4, 'Cars Customization' ),
     new Category(5, 'Bike Customization' ),
     new Category(6, 'Other' )
    ];
  }


  getSpecialization() {
   return [
     new Specialization(1, 1, 'Men Regular Formals' ),
     new Specialization(2, 1, 'Men Jackets, Suits' ),
     new Specialization(3, 1, 'Men Bridal Wear'),
     new Specialization(4, 1, 'Women Kurta & Gowns'),
     new Specialization(5, 1, 'Women Formals' ),
     new Specialization(6, 1, 'Women Blouse & Sarees'),
     new Specialization(7, 1, 'Women Salwar & Bridal Wear' ),

     new Specialization(8,  2, 'Men Regular Formals' ),
     new Specialization(9,  2, 'Men Regular Formals' ),
     new Specialization(10, 2, 'Men Jackets, Suits'),
     new Specialization(11, 2, 'Men Bridal Wear'),

     new Specialization(12, 3, 'Women Kurta & Gowns'),
     new Specialization(13, 3, 'Women Formals'),
     new Specialization(14, 3, 'Women Blouse & Sarees'),
     new Specialization(15, 3, 'Women Salwar & Bridal Wear'),

     new Specialization(16, 4, 'Seats'),
     new Specialization(17, 5, 'Others'),
     new Specialization(18, 6, 'Others')

    ];
  }

  getBusinessType() {
    return [
     new Businesstype(1, 'button' ),
     new Businesstype(2, 'fabric' ),
     new Businesstype(3, 'stiching' )
    ];
  }

  getBussinessCategory() {
    return [
      new Businesscategories(1, 1, 'material' ),
      new Businesscategories(2, 2, 'material' ),
      new Businesscategories(3, 3, 'service')
    ];
  }
}
