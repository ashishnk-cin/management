import { AgmCoreModule } from '@agm/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { FullPagesRoutingModule } from './full-pages-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TailorService } from 'app/services/tailor.service';
import { TailorListComponent } from './tailor-list/tailor-list.component';
import { KeysPipe } from 'app/pipes/keys.pipe';
import { FabricInputComponent } from './fabric-input/fabric-input.component';
import { PartnerComponent } from './partner/partner.component';
import { DropdownServiceService } from 'app/services/dropdown-service.service';

@NgModule({
    imports: [
        CommonModule,
        FullPagesRoutingModule,
        FormsModule,
        NgxDatatableModule,
        ReactiveFormsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyC8Bf196zavR1lYcmNUVTERp2tXfSOaBWI'
          })
    ],
    declarations: [
        DashboardComponent,
        TailorListComponent,
        KeysPipe,
        FabricInputComponent,
        PartnerComponent
    ],
    providers: [
        TailorService,
        DropdownServiceService
        ]
})
export class FullPagesModule { }
