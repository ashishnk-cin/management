import { Specialization } from './../../../model/specialization';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm, FormArray } from '@angular/forms';
import { TailorService } from 'app/services/tailor.service';
import { Category } from 'app/model/category';
import { DropdownServiceService } from 'app/services/dropdown-service.service';
import { Businesscategories } from 'app/model/businessCategory';

@Component({
  selector: 'app-partner',
  templateUrl: './partner.component.html',
  styleUrls: ['./partner.component.scss']
})
export class PartnerComponent implements OnInit {
  selectedCountry: Category = new Category(0, 'Tailoring');
  countries: Category[];
  states: Specialization[];

  selectedBusinessCategory: Category = new Category(0, 'stiching');
  businessType: Category[];
  businessCategory: Businesscategories[];

  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  tailorForm: FormGroup;
  lat: number;
  lng: number;
//   imagearry = new FormArray([]);

  constructor(
      private tailorService: TailorService,
      private router: Router,
      private _dataService: DropdownServiceService) {

    this.countries = this._dataService.getCategory();
    this.businessType = this._dataService.getBusinessType();
  }

  onSelect(categoryid) {
    this.states = this._dataService.getSpecialization().filter((item) => item.categoryid == categoryid);
  }

  onSelectType(businessid) {
    this.businessCategory = this._dataService.getBussinessCategory().filter((item) => item.categoryid == businessid);
  }

  ngOnInit() {

      this.getCurrentLocation();

      this.tailorForm = new FormGroup({

          // Basic Information
          '_name': new FormControl(null, [Validators.required]),
          'contactPerson': new FormControl(null, [Validators.required]),
          '_description': new FormControl(null, [Validators.required]),
          'designation': new FormControl(null, [Validators.required]),
          'contactNumber': new FormControl(null, [Validators.required, Validators.minLength(10)]),
          'emailTailor': new FormControl(null, [Validators.required, Validators.email]),
          'whatsappNumber': new FormControl(null, [Validators.required, Validators.minLength(10)]),
          'selectSpecialisation': new FormControl(null, []),
          'businessCategory': new FormControl(null, [Validators.required]),
           // Address Information
          '_address': new FormGroup({
          'selectState': new FormControl(null, [Validators.required]),
          'selectCity': new FormControl(null, [Validators.required]),
          'zipCode': new FormControl(null, [Validators.required, Validators.minLength(6)]),
          'fullAddress': new FormControl(null, [Validators.required]),
          }),

          // More Information
          'yearEstablished': new FormControl(null, [Validators.required]),
          'selectService': new FormControl(null, [Validators.required]),
          'daysRegularDelivery': new FormControl(null, [Validators.required, Validators.min(1)]),
          'daysExpressDelivery': new FormControl(null, [Validators.required, Validators.min(1)]),

          // Rate Card Costs
          '_products': new FormArray([]),

          // Personal Information
          '_personal': new FormGroup({
            'panCardNumber': new FormControl(null, [Validators.required, Validators.minLength(10)]),
            // 'panImage': new FormControl(null, [Validators.required]),
            'gstNumber': new FormControl(null, [Validators.required, Validators.minLength(15)]),
            'openToVisit': new FormControl(null, [Validators.required]),
            'noOfTailorWorking': new FormControl(null, [Validators.required, Validators.min(1)]),
            'agreeOneFreeTrail': new FormControl(null, [Validators.required]),
          }),

            // Bank Information
           '_bank': new FormGroup({
            'bankAccountNumber': new FormControl(null, [Validators.required, Validators.minLength(8)]),
            'bankIfscCode': new FormControl(null, [Validators.required, Validators.minLength(4)]),
            'bankName': new FormControl(null, [Validators.required]),
            'bankBranchName': new FormControl(null, [Validators.required])
            //    'bankCheckBookImage': new FormControl(null, [Validators.required])
           }),

            '_category': new FormControl(null, []),
            '_type': new FormControl(null, [Validators.required]),
            // '_images': this.imagearry
      });
  }


get _name() {
      return this.tailorForm.get('_name');
}

get contactPerson() {
      return this.tailorForm.get('contactPerson');
}

get contactNumber() {
    return this.tailorForm.get('contactNumber');
}

get whatsappNumber() {
    return this.tailorForm.get('whatsappNumber');
}

get zipCode() {
    return this.tailorForm.get('_address.zipCode');
}

// On cancel button pressed
onCancel() {
      this.tailorForm.reset();
}

onTemplateFormSubmit() {
      this.floatingLabelForm.reset();
}

get formData() {
    return <FormArray>this.tailorForm.get('_products');
 }

// Post method to submit the filled form
onSubmit() {
    console.log('This is Submitted Data:', this.tailorForm.value);
    if (!this.tailorForm.valid) {
        alert('Error Occured ! Please check if data entered are valid or some required fields are left Blank !');

    } else {
        this.tailorService.addTailor(this.tailorForm.value);
        alert('Tailor OnBoard Process Completed !');
        this.tailorForm.reset();
    }
}

// map location
onChangeLocation(event) {
    this.lat = event.coords.lat;
    this.lng = event.coords.lng;
    console.log(this.lat, this.lat);
}
// map location
getCurrentLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(position => {
            this.lat = position.coords.latitude;
            this.lng = position.coords.longitude;
        });
    }
}

// adding array of product data
onAddProducts() {
    // add new fields
    (<FormArray>this.tailorForm.get('_products')).push(
        new FormGroup({
            'productName': new FormControl(null, [Validators.required]),
            'productDescription': new FormControl(null, [Validators.required]),
            'productPrice': new FormControl(null, [Validators.required]),
            'productOffer': new FormControl(null, [Validators.required]),
            'productQuantity': new FormControl(null, [Validators.required]),
            'productImgLink': new FormControl(null, [Validators.required])
        })
    );
}

// onAddImage() {
//     (<FormArray>this.tailorForm.get('_images')).push(
//         new FormGroup({
//             'images': new FormControl(null, [Validators.required])
//         })
//     );
// }

}
