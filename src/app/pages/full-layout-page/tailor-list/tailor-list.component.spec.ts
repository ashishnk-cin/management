import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TailorListComponent } from './tailor-list.component';

describe('TailorListComponent', () => {
  let component: TailorListComponent;
  let fixture: ComponentFixture<TailorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TailorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TailorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
