import { TailorService } from './../../../services/tailor.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable/release';

declare var require: any;
const data: any = require('../../../model/company.json');


@Component({
  selector: 'app-tailor-list',
  templateUrl: './tailor-list.component.html',
  styleUrls: ['./tailor-list.component.scss'],
})

export class TailorListComponent implements OnInit {
  tailors$;
  rows = [];

  temp = [];

    // Table Column Titles
    columns = [
      { prop: 'name' },
      { name: 'Company' },
      { name: 'Gender' }
  ];

  @ViewChild(DatatableComponent) table: DatatableComponent;


  updateFilter(event) {
      const val = event.target.value.toLowerCase();

      // filter our data
      const temp = this.temp.filter(function (d) {
          return d.name.toLowerCase().indexOf(val) !== -1 || !val;
      });

      // update the rows
      this.rows = temp;
      // Whenever the filter changes, always go back to the first page
      this.table.offset = 0;
  }

  constructor(private tailorService: TailorService) {
   this.tailors$ = this.tailorService.getAll();
   this.temp = [...data];
        this.rows = data;
   }

  ngOnInit() {
  }

}
