import { AuthGuard } from 'app/services/auth-guard.service';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from 'app/pages/full-layout-page/dashboard/dashboard.component';
import { TailorListComponent } from 'app/pages/full-layout-page/tailor-list/tailor-list.component';
import { FabricInputComponent } from 'app/pages/full-layout-page/fabric-input/fabric-input.component';
import { PartnerComponent } from 'app/pages/full-layout-page/partner/partner.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    children: [
      {
        path: 'partner',
        component: PartnerComponent,
        data: {
          title: 'Partner Onboarding'
        }
      },
      {
        path: 'fabric-input',
        component: FabricInputComponent,
        data: {
          title: 'Fabric Input'
        }
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
          title: 'Dashboard'
        }
      },
      {
          path: 'tailor-list',
          component: TailorListComponent,
          data: {
            title: 'Tailor-List'
          }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FullPagesRoutingModule { }
