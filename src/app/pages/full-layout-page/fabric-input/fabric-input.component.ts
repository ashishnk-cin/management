import { Component, OnInit, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import {NgForm, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { TailorService } from 'app/services/tailor.service';

@Component({
  selector: 'app-fabric-input',
  templateUrl: './fabric-input.component.html',
  styleUrls: ['./fabric-input.component.scss']
})
export class FabricInputComponent implements OnInit {
  @ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  fabricForm: FormGroup;

  constructor(private tailorService: TailorService) {

    this.fabricForm = new FormGroup({

      '_category': new FormControl(null, [Validators.required]),
      '_type': new FormControl(null, [Validators.required]),
      // '_images': this.imagearry

      // Basic Information
      '_basicInfo': new FormGroup({
        '_name': new FormControl(null, [Validators.required]),
        'contactPerson': new FormControl(null, [Validators.required]),
        '_description': new FormControl(null, [Validators.required]),
        'designation': new FormControl(null, [Validators.required]),
        'contactNumber': new FormControl(null, [Validators.required, Validators.minLength(10)]),
        'emailPartner': new FormControl(null, [Validators.required, Validators.email]),
        'whatsappNumber': new FormControl(null, [Validators.required, Validators.minLength(10)]),
        'selectSpecialisation': new FormControl(null, [Validators.required]),
      }),

       // Address Information
      '_address': new FormGroup({
        'selectState': new FormControl(null, [Validators.required]),
        'selectCity': new FormControl(null, [Validators.required]),
        'zipCode': new FormControl(null, [Validators.required, Validators.minLength(6)]),
        'fullAddress': new FormControl(null, [Validators.required]),
      }),

      // Product Card and Costs
      '_products': new FormArray([]),

      // Service Information
      '_service': new FormGroup({
        'openToVisit': new FormControl(null, [Validators.required]),
        'selectService': new FormControl(null, [Validators.required]),
        'yearEstablished': new FormControl(null, [Validators.required]),
        'daysRegularDelivery': new FormControl(null, [Validators.required, Validators.min(1)]),
        'daysExpressDelivery': new FormControl(null, [Validators.required, Validators.min(1)]),
      }),

        // Document Information
       '_documents': new FormGroup({
        'panCardNumber': new FormControl(null, [Validators.required, Validators.minLength(10)]),
        'gstNumber': new FormControl(null, [Validators.required, Validators.minLength(15)]),
        'bankAccountNumber': new FormControl(null, [Validators.required, Validators.minLength(8)]),
        'bankIfscCode': new FormControl(null, [Validators.required, Validators.minLength(4)]),
        'bankName': new FormControl(null, [Validators.required]),
        'bankBranchName': new FormControl(null, [Validators.required])
       }),

  });
  }

  ngOnInit() {
    $.getScript('./assets/js/jquery.steps.min.js');
    $.getScript('./assets/js/wizard-steps.js');
}

get fabricFormData() {
  return <FormArray>this.fabricForm.get('_products');
}

// Post method to submit the filled form
// onSubmit() {
//   console.log('This is Submitted Data:', this.fabricForm.value);
//   if (!this.fabricForm.valid) {
//       alert('Error Occured ! Please check if data entered are valid or some required fields are left Blank !');

//   } else {
//       this.tailorService.addTailor(this.fabricForm.value);
//       alert('Tailor OnBoard Process Completed !');
//       this.fabricForm.reset();
//   }
// }

onTemplateFormSubmit() {
  this.floatingLabelForm.reset();
}

onAddProducts() {
  // add new fields
  (<FormArray>this.fabricForm.get('_products')).push(
      new FormGroup({
          'productName': new FormControl(null, [Validators.required]),
          'productDescription': new FormControl(null, [Validators.required]),
          'productPrice': new FormControl(null, [Validators.required]),
          'productOfferPercent': new FormControl(null, [Validators.required]),
          'productQuantity': new FormControl(null, [Validators.required])
      })
  );
}

}
