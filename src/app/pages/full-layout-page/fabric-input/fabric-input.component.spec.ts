import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FabricInputComponent } from './fabric-input.component';

describe('FabricInputComponent', () => {
  let component: FabricInputComponent;
  let fixture: ComponentFixture<FabricInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FabricInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FabricInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
