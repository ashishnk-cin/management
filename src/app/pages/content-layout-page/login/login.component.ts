import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AuthService } from 'app/services/auth.service';
import { User } from 'app/model/user-model';
import { FormGroup } from '@angular/forms';
import { error } from 'selenium-webdriver';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    invalidLogin: Boolean = false;
    user: User;
    returnUrl: string;
    regularForm: FormGroup;

  @ViewChild('f') loginForm: NgForm;

    constructor(private router: Router,
        private route: ActivatedRoute,
        private authService: AuthService) { }

    // On submit button click
    onCancel() {
        this.loginForm.reset();
    }
    // On Forgot password link click
    onForgotPassword() {
        this.router.navigate(['/forgotpassword'], { relativeTo: this.route.parent });
    }
    // On registration link click
    onRegister() {
        this.router.navigate(['/register'], { relativeTo: this.route.parent });
    }

  ngOnInit() {
      // reset login status
    this.authService.logout();

  }

  login(credentials) {
    console.log('Login Component value:', credentials);
    this.authService.loginA(credentials)
        .subscribe(result => {
            if (result)  {
            this.router.navigate(['/dashboard']);
            console.log(result);
            } else {
                console.log('result is false');
                this.invalidLogin = true;
            }
        });
    }
}
